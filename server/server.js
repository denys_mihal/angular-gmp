const express = require('express');
const path = require('path');

const app = express();
const publicPath = path.resolve('../my-project/dist/my-project');

app.use(express.static(publicPath));

app.get('/', (req, res) => {
    res.sendFile('index.html');
});

app.listen(8080, () => {
    console.log('Magic is happening on localhost:8080');
});