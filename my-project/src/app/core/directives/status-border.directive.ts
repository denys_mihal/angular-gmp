import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

const TWO_WEEKS_MS = 14 * 24 * 60 * 60 * 1000;

@Directive({
    selector: '[appStatusBorder]'
})
export class StatusBorderDirective implements AfterViewInit {
    @Input('appStatusBorder') private creationDate: string;

    constructor(private elementRef: ElementRef) {
    }

    private getStatusColor(): string {
        const creationDate = Date.parse(this.creationDate);
        const today = Date.now();

        if (creationDate < today && creationDate >= today - TWO_WEEKS_MS) {
            return 'green';
        }

        if (creationDate > today) {
            return 'blue';
        }

        return 'transparent';
    }

    ngAfterViewInit() {
        this.elementRef.nativeElement.style.borderColor = this.getStatusColor();
    }

}
