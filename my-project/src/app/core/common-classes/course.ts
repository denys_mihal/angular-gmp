import { CourseModel } from '../interfaces/course.model';

export class Course implements CourseModel {
    constructor(
        public id: number,
        public title: string,
        public creationDate: Date | string,
        public durationMinutes: number,
        public description: string,
        public topRated: boolean,
        public authors: any[]
    ) { }
}

export function courseFactory({
    id,
    name,
    description,
    isTopRated,
    date,
    authors,
    length
}): Course {
    authors = authors.map(({
        id,
        name,
        lastName
    }) => {
        const fullName = `${name} ${lastName}`;
        return {
            id,
            name: fullName,
            display: fullName
        }
    })
    return {
        id,
        title: name,
        creationDate: date,
        durationMinutes: length,
        description,
        topRated: isTopRated,
        authors
    }
}
