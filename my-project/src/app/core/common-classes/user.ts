import { UserEntityModel } from '../interfaces/user-entity.model';

export class User implements UserEntityModel {
    fullName: string;

    constructor(
        public id: number,
        public firstName: string,
        public lastName: string
    ) {
        this.fullName = `${this.firstName} ${this.lastName}`;
    }
}
