import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

const URL_PREFIX = 'http://localhost:3004';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationService {
    userInformation: {login: string, token: string}

    constructor(
        private router: Router,
        private http: HttpClient,
        private _store: Store<{userInformation: {login: string, token: string}}>
    ) {
        _store.select('userInformation')
            .subscribe((nextUserInformation) => {
                return this.userInformation = nextUserInformation;
            })
    }

    login(credentials): void {
        this.http.post(`${URL_PREFIX}/auth/login`, credentials)
            .subscribe(({token}: any) => {
                this._store.dispatch({
                    type: 'USER_INFORMATION_SET',
                    payload: {
                        login: credentials.login,
                        token
                    }
                });
                this.router.navigate(['courses']);
            });
    }

    logout(): void {
        this._store.dispatch({
            type: 'USER_INFORMATION_REMOVE'
        });
        this.router.navigate(['login']);
    }

    isAuthorized(): boolean {
        return !!this.userInformation.token;
    }

    getUserInfo(): Observable<any> {
        return this.http.post(`${URL_PREFIX}/auth/userinfo`, {token: this.userInformation.token});
    }
}
