import { Pipe, PipeTransform } from '@angular/core';

const DIRECTION = {
    ASC: 'asc',
    DESC: 'desc'
};

@Pipe({
    name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

    transform(arr: any[], prop: string, dir: string = DIRECTION.ASC): any {
        if (!arr.length) {
            return;
        }
        const direction = dir === DIRECTION.DESC ? -1 : 1;
        const first = arr[0][prop];

        switch (typeof first) {
            case 'number': {
                return this.getSortedNumbers(arr, prop, direction);
            }
            case 'boolean': {
                return this.getSortedBooleans(arr, prop, direction);
            }
            case 'string': {
                const date = Date.parse(first);

                return date
                    ? this.getSortedDates(arr, prop, direction)
                    : this.getSortedStrings(arr, prop, direction);
            }

        }
    }

    private getSortedNumbers(arr: any[], prop: string, dir: number): any[] {
        return arr.sort((prev, next) => (prev[prop] - next[prop]) * dir);
    }

    private getSortedBooleans(arr: any[], prop: string, dir: number): any[] {
        return arr.sort((prev, next) => next[prop] ? dir : -dir);
    }

    private getSortedDates(arr: any[], prop: string, dir: number): any[] {
        return arr.sort((prev, next) => (Date.parse(prev[prop]) - Date.parse(next[prop])) * dir);
    }

    private getSortedStrings(arr: any[], prop: string, dir: number): any[] {
        return arr.sort((prev, next) => prev[prop] > next[prop] ? dir : -dir);
    }
}