import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

    transform(arr: any[], prop: string, value: string): any[] {
        return arr.filter((el) => RegExp(value, 'i').test(el[prop]));
    }
}
