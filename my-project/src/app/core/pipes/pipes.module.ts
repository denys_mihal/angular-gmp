import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DurationPipe } from './duration.pipe';
import { FilterByPipe } from './filter-by.pipe';

@NgModule({
    declarations: [
        DurationPipe,
        FilterByPipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        DurationPipe,
        FilterByPipe
    ]
})
export class PipesModule { }
