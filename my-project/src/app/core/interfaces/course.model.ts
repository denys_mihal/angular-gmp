export interface CourseModel {
    id: number;
    title: string;
    creationDate: Date | string;
    durationMinutes: number;
    description: string;
    topRated: boolean;
}
