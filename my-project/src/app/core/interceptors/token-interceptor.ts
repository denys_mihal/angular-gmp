import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { LoadingSpinnerService } from 'src/app/home-page/loading-spinner/loading-spinner.service';
import { finalize } from 'rxjs/operators';
import { AuthorizationService } from '../auth/authorization.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private loadingSpinnerService: LoadingSpinnerService,
        private authService: AuthorizationService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loadingSpinnerService.show();

        const paramReq = this.authService.userInformation.token
            ? req.clone(this.generateInfoToClone(req))
            : req.clone();

        return next.handle(paramReq).pipe(
            finalize(() => this.loadingSpinnerService.hide())
        );
    }

    private generateInfoToClone(req) {
        return {
            params: req.params.set(
                'token',
                this.authService.userInformation.token
            )
        }
    }
}
