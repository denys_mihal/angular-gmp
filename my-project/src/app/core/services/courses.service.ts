import { Injectable } from '@angular/core';
import { Course, courseFactory } from '../common-classes/course';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

// tslint:disable-next-line: max-line-length
export const fakeDescription = 'Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college\'s classes. They\'re published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.';

const URL_PREFIX = 'http://localhost:3004';

@Injectable({
    providedIn: 'root'
})
export class CoursesService {
    private coursesList: Course[];

    constructor(
        private router: Router,
        private http: HttpClient,
        private _store: Store<{coursesList: Course[]}>
    ) {
        _store.select('coursesList')
            .subscribe((updatedCoursesList) => this.coursesList = updatedCoursesList);
    }

    getList(): Course[] {
        return [...this.coursesList];
    }

    createCourse(item): any {
        return this.http.post(`${URL_PREFIX}/courses`, this.getModifiedItem(item))
            .pipe(
                map((newCourse: any) => {
                    this._store.dispatch({
                        type: 'COURSES_LIST_CREATE_COURSE',
                        payload: {newCourse}
                    });
                    return this.coursesList;
                })
            );
    }

    editCourse(item): any {
        return this.http.patch(`${URL_PREFIX}/courses/${item.id}`, this.getModifiedItem(item))
            .pipe(
                map((newCourse: any) => {
                    this._store.dispatch({
                        type: 'COURSES_LIST_EDIT_COURSE',
                        payload: {newCourse}
                    });
                    return this.coursesList;
                })
            );
    }

    private getModifiedItem(item) {
        const {
            id,
            title: name,
            creationDate: date,
            durationMinutes: length,
            description,
            authors
        } = item;
        return {
            id,
            name,
            description,
            isTopRated: false,
            date,
            authors,
            length
        };
    }

    getItemById(id: number): Course {
        return this.coursesList.find((el) => el.id === id);
    }

    navigateToEditPage(item: Course): void {
        this.router.navigate(['courses', item.id], {queryParams: {pageTitle: 'Edit'}});
    }

    removeItem(id: number): any {
        if (!confirm('Do you really want to delete this course? OK/Cancel')) {
            return of(true);
        }
        return this.http.delete(`${URL_PREFIX}/courses/${id}`);
    }

    private getCoursesRequest(params): Observable<any> {
        return this.http.get(`${URL_PREFIX}/courses`, {params});
    }

    loadMoreCourses() {
        return this.getCourses(true);
    }

    searchCourse(textFragment = '') {
        const params = textFragment
            ? {
                textFragment
            }
            : {};
        return this.getCoursesRequest(params)
            .pipe(
                map((resp: any[]) => {
                    this._store.dispatch({
                        type: 'COURSES_LIST_REWRITE',
                        payload: {
                            newList: resp.map(courseFactory)
                        }
                    });
                    return this.coursesList;
                })
            );
    }

    getCourses(savePreviousCoursesList) {
        const params = {
            start: '0',
            count: '5'
        }
        return this.getCoursesRequest(params)
            .pipe(
                map((resp: any[]) => {
                    this._store.dispatch({
                        type: 'COURSES_LIST_UPDATE',
                        payload: {
                            savePreviousCoursesList,
                            newList: resp.map(courseFactory)
                        }
                    });
                    return this.coursesList;
                })
            );
    }

    getAuthorsList() {
        return this.http.get(`${URL_PREFIX}/authors`);
    }
}
