import { NgModule } from '@angular/core';
import { CoursesListComponent } from './home-page/courses-list/courses-list.component';
import { RouterModule, Routes } from '@angular/router';
import { CourseDetailsComponent } from './home-page/course-details/course-details.component';
import { NotFoundComponent } from './home-page/not-found/not-found.component';
import { AuthGuard } from './core/auth/auth.guard';
import { LoginComponent } from './home-page/login/login.component';

const routes: Routes = [{
    path: '',
    redirectTo: 'courses',
    pathMatch: 'full'
},{
    path: 'login',
    component: LoginComponent
},{
    path: 'courses',
    canActivate: [AuthGuard],
    component: CoursesListComponent
}, {
    path: 'courses/:id',
    canActivate: [AuthGuard],
    component: CourseDetailsComponent
}, {
    path: 'courses/new',
    canActivate: [AuthGuard],
    component: CourseDetailsComponent
}, {
    path: '**',
    component: NotFoundComponent
}];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
