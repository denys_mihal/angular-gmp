import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesListModule } from './courses-list/courses-list.module';
import { LoginModule } from './login/login.module';
import { AddCourseModule } from './course-details/course-details.module';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FilterByPipe } from '../core/pipes';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';


@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        NotFoundComponent,
        LoadingSpinnerComponent
    ],
    imports: [
        CommonModule,
        CoursesListModule
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        LoadingSpinnerComponent,
        LoginModule,
        AddCourseModule
    ],
    providers: [FilterByPipe]
})
export class HomePageModule { }
