import { Component, OnInit, Input, forwardRef} from '@angular/core';
import {
    FormGroup,
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormBuilder,
    Validators,
    AbstractControl,
    ValidationErrors,
    Validator
} from '@angular/forms';
import { TagModel } from 'ngx-chips/core/accessor';
import { Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-authors-input',
    templateUrl: './authors-input.component.html',
    styleUrls: ['./authors-input.component.sass'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AuthorsInputComponent),
        multi: true
    }, {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => AuthorsInputComponent),
        multi: true
    }]
})
export class AuthorsInputComponent implements OnInit, ControlValueAccessor, Validator {
    @Input() hasError: Function;
    @Input() validationConfig: any;
    @Input() authorsList: any[];

    authorsForm: FormGroup;

    constructor(private fb: FormBuilder) {}

    get authors() {return this.authorsForm.get('authors')};

    ngOnInit() {
        this.authorsForm = this.fb.group({
            'authors': ['', [Validators.required]]
        });
    }

    onTouched: () => void = () => { };

    writeValue(value: any): void {
        if (!value) {
            return;
        };
        this.authorsForm.controls.authors.setValue(value);
    }

    registerOnChange(fn: any): void {
        this.authorsForm.valueChanges.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    validate(c: AbstractControl): ValidationErrors | null {
        return this.authorsForm.valid ? null : { invalidForm: { valid: false, message: "authorsForm fields are invalid" } };
    }

    handleProgramaticErrorCheck() {
        const cloudTagsInput = document.getElementById('authors');
        if (this.hasError(this.authorsForm.controls.authors)) {
            cloudTagsInput.classList.add('border-danger');
        } else {
            cloudTagsInput.classList.remove('border-danger');
        };
    }

    onInputBlurred() {
        this.handleProgramaticErrorCheck();
    }

    onAdding() {
        this.handleProgramaticErrorCheck();
    }

    onRemoving(tag: TagModel): Observable<TagModel> {
        this.handleProgramaticErrorCheck();
        return of(tag)
            .pipe(filter(() => true));
    }
}
