import { Component, OnInit, Input, forwardRef } from '@angular/core';
import {
    FormGroup,
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormBuilder,
    Validators,
    AbstractControl,
    ValidationErrors,
    Validator
} from '@angular/forms';

@Component({
    selector: 'app-date-input',
    templateUrl: './date-input.component.html',
    styleUrls: ['./date-input.component.sass'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DateInputComponent),
        multi: true
    }, {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => DateInputComponent),
        multi: true
    }]
})
export class DateInputComponent implements OnInit, ControlValueAccessor, Validator {
    @Input() hasError: Function;
    @Input() validationConfig: any;

    creationDateForm: FormGroup;

    constructor(private fb: FormBuilder) {}

    get creationDate() {return this.creationDateForm.get('creationDate')};

    ngOnInit() {
        this.creationDateForm = this.fb.group({
            'creationDate': ['', [Validators.required, Validators.pattern(this.validationConfig.datePattern)]]
        });
    }

    onTouched: () => void = () => { };

    writeValue(value: any): void {
        if (!value) {
            return;
        };
        const modifiedDate = this.getModifiedDate(value);
        this.creationDateForm.controls.creationDate.setValue(modifiedDate);
    }

    private getModifiedDate(value) {
        const date = new Date(value);
        const month = date.getMonth() + 1
        const fullMonth = month < 10
            ? `0${month}`
            : month

        return `${date.getDate()}/${fullMonth}/${date.getFullYear()}`;
    }

    registerOnChange(fn: any): void {
        this.creationDateForm.valueChanges.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    validate(c: AbstractControl): ValidationErrors | null {
        return this.creationDateForm.valid ? null : { invalidForm: { valid: false, message: "creationDateForm fields are invalid" } };
    }
}
