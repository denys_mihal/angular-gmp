import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from 'src/app/core/services';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-course-details',
    templateUrl: './course-details.component.html',
    styleUrls: ['./course-details.component.sass']
})
export class CourseDetailsComponent implements OnInit, OnDestroy {
    private paramsSubscription: Subscription;
    private pageTitle: string;
    private id: number;
    authorsList: [];
    courseDetailsForm: FormGroup;
    item = {
        id: null,
        title: '',
        creationDate: '',
        durationMinutes: 0,
        description: '',
        topRated: false,
        authors: []
    };
    validationConfig = {
        titleMaxLength: 50,
        descriptionMaxLength: 500,
        datePattern: '(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/(19[0-9][0-9]|20[0-1][0-9]|2020)',
        durationPattern: '^[0-9]+$'
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private coursesService: CoursesService,
        private fb: FormBuilder
    ) {
        this.paramsSubscription = this.route.params.subscribe((params) => this.id = +params.id);
        this.handleItemConstruction();
    }

    get title() {return this.courseDetailsForm.get('title')};
    get description() {return this.courseDetailsForm.get('description')};
    get creationDate() {return this.courseDetailsForm.get('creationDate')};
    get durationMinutes() {return this.courseDetailsForm.get('durationMinutes')};
    get authors() {return this.courseDetailsForm.get('authors')};

    ngOnInit() {
        this.courseDetailsForm = this.fb.group({
            'title': ['', [Validators.required, Validators.maxLength(this.validationConfig.titleMaxLength)]],
            'description': ['', [Validators.required, Validators.maxLength(this.validationConfig.descriptionMaxLength)]],
            'creationDate': [''],
            'durationMinutes': [''],
            'authors': ['']
        });
        this.getAuthorsList();
    }

    onSubmit(): void {
        const method = this.isEditMode()
            ? 'editCourse'
            : 'createCourse'

        const serverItem = this.getServerItem(this.courseDetailsForm);
        serverItem.id = this.item.id;
        this.coursesService[method](serverItem).subscribe(() => this.router.navigate(['courses']));
    }

    handleCancel(): void {
        this.router.navigate(['courses']);
    }

    getPageTitle() {
        return `${this.pageTitle || 'New'} course`;
    }

    private handleItemConstruction() {
        this.pageTitle = this.route.snapshot.queryParams.pageTitle;
        if (this.isEditMode()) {
            this.handleCourseEdit();
        }
    }

    private handleCourseEdit() {
        const editedItem = this.coursesService.getItemById(this.id);
        Object.assign(this.item, editedItem);
    }

    ngOnDestroy() {
        this.paramsSubscription.unsubscribe();
    }

    private isEditMode() {
        return this.pageTitle === 'Edit';
    }

    hasError(control) {
        return control.invalid && control.touched;
    }

    getServerItem({value}) {
        const result: {[key: string]: any} = {};

        Object.keys(value).forEach((key) => {
            if (typeof value[key] === 'object') {
                result[key] = value[key][key];
                return;
            }
            result[key] = value[key];
        });

        const [day, month, year] = result.creationDate.split('/');
        result.creationDate = `${month}/${day}/${year}`;

        result.authors.forEach((author) => {
            const [name, lastName] = author.name.split(' ');
            author.name = name;
            author.lastName = lastName;
            delete author.display;
            delete author.value;
        });

        return result;
    }

    getAuthorsList() {
        this.coursesService.getAuthorsList()
            .subscribe((authors: []) => this.authorsList = authors);
    }
}
