import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseDetailsComponent } from './course-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateInputComponent } from './date-input/date-input.component';
import { DurationInputComponent } from './duration-input/duration-input.component';
import { AuthorsInputComponent } from './authors-input/authors-input.component';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { BreadcrumbsModule } from '../breadcrumbs/breadcrumbs.module';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
    declarations: [
        CourseDetailsComponent,
        DateInputComponent,
        DurationInputComponent,
        AuthorsInputComponent
    ],
    imports: [
        BreadcrumbsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PipesModule,
        TagInputModule,
        BrowserAnimationsModule,
    ],
    exports: [
        CourseDetailsComponent
    ]
})
export class AddCourseModule { }
