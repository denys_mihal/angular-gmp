import { Component, OnInit, Input, forwardRef } from '@angular/core';
import {
    FormGroup,
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormBuilder,
    Validators,
    AbstractControl,
    ValidationErrors,
    Validator
} from '@angular/forms';

@Component({
    selector: 'app-duration-input',
    templateUrl: './duration-input.component.html',
    styleUrls: ['./duration-input.component.sass'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DurationInputComponent),
        multi: true
    }, {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => DurationInputComponent),
        multi: true
    }]
})
export class DurationInputComponent implements OnInit, ControlValueAccessor, Validator {
    @Input() hasError: Function;
    @Input() validationConfig: any;

    durationMinutesForm: FormGroup;

    constructor(private fb: FormBuilder) {}

    get durationMinutes() {return this.durationMinutesForm.get('durationMinutes')};

    ngOnInit() {
        this.durationMinutesForm = this.fb.group({
            'durationMinutes': ['', [Validators.required, Validators.pattern(this.validationConfig.durationPattern)]]
        });
    }

    onTouched: () => void = () => { };

    writeValue(value: any): void {
        if (!value) {
            return;
        };
        this.durationMinutesForm.controls.durationMinutes.setValue(value);
    }

    registerOnChange(fn: any): void {
        this.durationMinutesForm.valueChanges.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    validate(c: AbstractControl): ValidationErrors | null {
        return this.durationMinutesForm.valid ? null : { invalidForm: { valid: false, message: "durationMinutesForm fields are invalid" } };
    }

}
