import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.sass']
})
export class BreadcrumbsComponent implements OnInit {
    @Input() courseTitle: string;
    @Input() clickable: boolean;

    constructor() { }

    ngOnInit() {
    }

    getCoursesTitle() {
        return this.courseTitle ? `/ ${this.courseTitle}` : '';
    }
}
