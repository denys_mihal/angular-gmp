const initialState = [];

const _todos = (state = initialState, action) => {
    switch (action.type) {
        case 'COURSES_LIST_REWRITE':
            return [...action.payload.newList];

        case 'COURSES_LIST_UPDATE':
            return [
                ...action.payload.savePreviousCoursesList ? state : [],
                ...action.payload.newList
            ];

        case 'COURSES_LIST_CREATE_COURSE':
            state.push(action.payload.newCourse)
            return state;

        case 'COURSES_LIST_EDIT_COURSE':
            const editedCourse = state.find(({id}) => id === action.payload.newCourse.id);
            Object.assign(editedCourse, action.payload.newCourse);
            return state;

        default:
            return state;
    }
}

export function coursesListReducer(state, action) {
    return _todos(state, action);
}
