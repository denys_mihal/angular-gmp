const initialState = {
    login: '',
    token: ''
}

const _todos = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_INFORMATION_SET':
            return Object.assign(state, action.payload);

        case 'USER_INFORMATION_REMOVE':
            return initialState;

        default:
            return state;
    }
}

export function userInformationReducer(state, action) {
    return _todos(state, action);
}
