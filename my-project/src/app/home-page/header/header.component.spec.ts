import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HeaderComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        spyOn(console, 'log');
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('#handleLogoClick', () => {
        it('should log a message to console', () => {
            const logoClickMessage = 'Logo button was clicked!';
            component.handleLogoClick();
            expect(console.log).toHaveBeenCalledWith(logoClickMessage);
        });
    });

    describe('#handleUserLogOff', () => {
        it('should log a message to console', () => {
            const logOffMessage = '"Log off" button was clicked!';
            component.handleUserLogOff();
            expect(console.log).toHaveBeenCalledWith(logOffMessage);
        });
    });
});
