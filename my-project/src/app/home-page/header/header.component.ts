import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/core/auth/authorization.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
    userInfo: string;
    private isUserInfoIntialized: boolean;

    constructor(private authService: AuthorizationService, private router: Router) {
    }

    ngOnInit() {
    }

    handleLogoClick(): void {
        this.router.navigate(['courses']);
    }

    handleUserLogOff(): void {
        this.authService.logout();
    }

    isAuthorized(): boolean {
        const isAuthorized = this.authService.isAuthorized();
        if (!this.isUserInfoIntialized && isAuthorized) {
            this.getUserInfo();
            this.isUserInfoIntialized = true;
        }
        return isAuthorized;
    }

    private getUserInfo() {
        this.authService.getUserInfo()
            .subscribe(({name}) => this.userInfo = `${name.first} ${name.last}`);
    }
}
