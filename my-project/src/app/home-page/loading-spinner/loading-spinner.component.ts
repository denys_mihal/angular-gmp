import { Component, OnInit } from '@angular/core';
import { LoadingSpinnerService } from './loading-spinner.service';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-loading-spinner',
    templateUrl: './loading-spinner.component.html',
    styleUrls: ['./loading-spinner.component.sass']
})
export class LoadingSpinnerComponent implements OnInit {
    isLoading: Subject<boolean> = this.loadingSpinnerService.isLoading;

    constructor(private loadingSpinnerService: LoadingSpinnerService) { }

    ngOnInit() {
    }
}