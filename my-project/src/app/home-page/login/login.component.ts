import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/core/auth/authorization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;

    constructor(private authService: AuthorizationService, private fb: FormBuilder) { }

    get login() {return this.loginForm.get('login')};
    get password() {return this.loginForm.get('password')};

    ngOnInit() {
        this.loginForm = this.fb.group({
            'login': ['', [Validators.required]],
            'password': ['', [Validators.required]]
        });
    }

    onSubmit(): void {
        this.authService.login(this.loginForm.value);
    }

    hasError(control) {
        return control.invalid && control.touched;
    }
}
