import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { CoursesListComponent, fakeDescription } from './courses-list.component';
import { CourseItemComponent } from './course-item/course-item.component';
import { Course } from 'src/app/core/common-classes/course';
import { StatusBorderDirective } from '../../core/directives/index';
import { OrderByPipe, DurationPipe, FilterByPipe } from '../../core/pipes/index';

describe('CoursesListComponent', () => {
    let component: CoursesListComponent;
    let fixture: ComponentFixture<CoursesListComponent>;

    let filterByPipe: any;

    let initialCoursesList: Course[];
    let filteredCoursesList: Course[];

    beforeEach(async(() => {
        initialCoursesList = [
            new Course(1, 'Custom title', '10-08-2019', 71, fakeDescription, false),
            new Course(2, 'Another custom title', '12-08-2019', 143, fakeDescription, true),
            new Course(3, 'Some more custom title', '11-08-2019', 49, fakeDescription, false)
        ];
        filteredCoursesList = [
            new Course(3, 'Some more custom title', '11-08-2019', 49, fakeDescription, false)
        ];

        filterByPipe = jasmine.createSpyObj('FilterByPipe', ['transform']);
        filterByPipe.transform.and.returnValue(filteredCoursesList);

        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [
                CoursesListComponent,
                CourseItemComponent,
                OrderByPipe,
                StatusBorderDirective,
                DurationPipe
            ],
            providers: [
                {provide: FilterByPipe, useValue: filterByPipe}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CoursesListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        spyOn(console, 'log');
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        it('should initialize an empty value to search query property', () => {
            component.ngOnInit();
            expect(component.searchQuery).toBe('');
        });
        it('should set the initial list of the courses', () => {
            component.ngOnInit();
            expect(component['initialCoursesList']).toEqual(initialCoursesList);
        });

        it('should set the filtered list of the courses', () => {
            component.ngOnInit();
            expect(component.filteredCoursesList).toEqual(initialCoursesList);
        });
    });

    describe('#handleAddCourse', () => {
        it('should log a message to console', () => {
            const addButtonClickMessage = '"Add course" button was clicked!';
            component.handleAddCourse();
            expect(console.log).toHaveBeenCalledWith(addButtonClickMessage);
        });
    });

    describe('#handleEditItem', () => {
        it('should log a message to console', () => {
            const id = 42;
            const editButtonClickMessage = `"Edit" button was clicked! Course number ${id} will be edited`;
            component.handleEditItem(id);
            expect(console.log).toHaveBeenCalledWith(editButtonClickMessage);
        });
    });

    describe('#handleDeleteItem', () => {
        it('should log a message to console', () => {
            const id = 42;
            const deleteButtonClickMessage = `"Delete" button was clicked! Course number ${id} will be deleted`;
            component.handleDeleteItem(id);
            expect(console.log).toHaveBeenCalledWith(deleteButtonClickMessage);
        });
    });

    describe('#handleLoadMore', () => {
        it('should log a message to console', () => {
            const loadMoreButtonClickMessage = '"Load more" button was clicked!';
            component.handleLoadMore();
            expect(console.log).toHaveBeenCalledWith(loadMoreButtonClickMessage);
        });
    });

    describe('#handleSearch', () => {
        it('should set new filtered courses list', () => {
            component.handleSearch();
            expect(component.filteredCoursesList).toEqual(filteredCoursesList);
        });

        it('should filter current previously filtered courses list', () => {
            const searQuery = component.searchQuery = 'some';
            component.handleSearch();
            expect(filterByPipe.transform).toHaveBeenCalledWith(initialCoursesList, 'title', searQuery);
        });
    });
});
