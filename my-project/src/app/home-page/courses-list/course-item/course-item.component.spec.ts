import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CourseItemComponent } from './course-item.component';
import { Course } from 'src/app/core/common-classes/course';
import { DurationPipe } from '../../../core/pipes';
import { StatusBorderDirective } from '../../../core/directives';

describe('CourseItemComponent', () => {
    let component: CourseItemComponent;
    let fixture: ComponentFixture<CourseItemComponent>;

    let expectedItem;

    beforeEach(async(() => {
        expectedItem = new Course(123, 'title', '10-08-2019', 123, 'description', false);

        TestBed.configureTestingModule({
            declarations: [
                CourseItemComponent,
                DurationPipe,
                StatusBorderDirective
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CourseItemComponent);
        component = fixture.componentInstance;
        component.courseItem = expectedItem;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('#handleEditCourse', () => {
        it('should emit an event', () => {
            spyOn(component.editItem, 'emit');
            const id = component.courseItem.id = 42;
            component.handleEditCourse();
            expect(component.editItem.emit).toHaveBeenCalledWith(id);
        });
    });

    describe('#handleDeleteCourse', () => {
        it('should emit an event', () => {
            spyOn(component.deleteItem, 'emit');
            const id = component.courseItem.id = 42;
            component.handleDeleteCourse();
            expect(component.deleteItem.emit).toHaveBeenCalledWith(id);
        });
    });

    describe('@Input', () => {
        it('should display course id and title', () => {
            const {id, title} = component.courseItem;
            const expectedText = `Video Course ${id}. ${title.toUpperCase()}`;
            const courseDe = fixture.debugElement.query(By.css('.video-course-name'));
            const courseEl = courseDe.nativeElement;
            expect(courseEl.textContent).toContain(expectedText);
        });
    });

    describe('@Output', () => {
        it('should raise editItem event when edit button id clicked', () => {
            const courseDe = fixture.debugElement.query(By.css('.course-actions-section .edit-button'));
            let editItemId: number;
            component.editItem.subscribe((id: number) => editItemId = id);
            courseDe.triggerEventHandler('click', null);
            expect(editItemId).toBe(expectedItem.id);
        });

        it('should raise deleteItem event when delete button id clicked', () => {
            const courseDe = fixture.debugElement.query(By.css('.course-actions-section .delete-button'));
            let deleteItemId: number;
            component.deleteItem.subscribe((id: number) => deleteItemId = id);
            courseDe.triggerEventHandler('click', null);
            expect(deleteItemId).toBe(expectedItem.id);
        });
    });
});
