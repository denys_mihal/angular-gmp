import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseItemComponent } from './course-item.component';
import { StatusBorderDirective } from 'src/app/core/directives';
import { PipesModule } from 'src/app/core/pipes/pipes.module';

@NgModule({
    declarations: [
        CourseItemComponent,
        StatusBorderDirective
    ],
    imports: [
        CommonModule,
        PipesModule
    ],
    exports: [CourseItemComponent]
})
export class CourseItemModule { }
