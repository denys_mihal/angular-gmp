import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Course } from 'src/app/core/common-classes/course';

@Component({
    selector: 'app-course-item',
    templateUrl: './course-item.component.html',
    styleUrls: ['./course-item.component.sass'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemComponent implements OnInit {
    @Input() courseItem: Course;
    @Output() editItem: EventEmitter<Course> = new EventEmitter<Course>();
    @Output() deleteItem: EventEmitter<number> = new EventEmitter<number>();

    constructor() { }

    ngOnInit() {
    }

    handleEditCourse(): void {
        this.editItem.emit(this.courseItem);
    }

    handleDeleteCourse(): void {
        this.deleteItem.emit(this.courseItem.id);
    }
}
