// tslint:disable-next-line: max-line-length
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Course } from 'src/app/core/common-classes/course';
import { CoursesService } from 'src/app/core/services';
import { Subscription, Subject, of } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, filter, mergeMap, delay } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-courses-list',
    templateUrl: './courses-list.component.html',
    styleUrls: ['./courses-list.component.sass']
})
export class CoursesListComponent implements OnInit, OnDestroy {
    searchQuerySubs: Subscription;
    filteredCoursesList: Course[] = [];
    searchKeyUp = new Subject<KeyboardEvent>();
    searchForm: FormGroup

    constructor(
        private coursesService: CoursesService,
        private fb: FormBuilder
    ) {
        this.initiateSearchKeyUpSubscription();
    }

    ngOnInit() {
        this.searchForm = this.fb.group({
            'searchQuery': ['']
        });
        this.getCourses();
    }

    get searchQuery() {return this.searchForm.get('searchQuery')};

    private getCourses() {
        this.coursesService.getCourses(false)
            .subscribe((resp) => this.filteredCoursesList = resp);
    }

    handleEditItem(item: Course): void {
        this.coursesService.navigateToEditPage(item);
    }

    handleDeleteItem({id}: Course): void {
        this.coursesService.removeItem(id)
            .subscribe((resp) => {
                if (typeof resp === 'object') {
                    this.getCourses();
                }
            });
    }

    handleLoadMore(): void {
        this.coursesService.loadMoreCourses()
            .subscribe((resp) => this.filteredCoursesList = resp);
    }

    initiateSearchKeyUpSubscription(): void {
        const searchQueryObs = this.searchKeyUp.pipe(
            startWith(''),
            filter((value: string) => value.length >= 3),
            debounceTime(500),
            distinctUntilChanged(),
            mergeMap(search => of(search).pipe(
                delay(100),
            ))
        );

        this.searchQuerySubs = searchQueryObs.subscribe({
            next: (query: string) => this.coursesService.searchCourse(query)
                .subscribe((resp) => this.filteredCoursesList = resp)
        });
    }

    ngOnDestroy() {
        this.searchQuerySubs.unsubscribe();
    }
}
