import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderByPipe } from '../../core/pipes';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { CourseItemModule } from './course-item/course-item.module';
import { CoursesListComponent } from './courses-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '../breadcrumbs/breadcrumbs.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        CoursesListComponent,
        OrderByPipe
    ],
    imports: [
        RouterModule,
        BreadcrumbsModule,
        CommonModule,
        CourseItemModule,
        PipesModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        CourseItemModule
    ]
})
export class CoursesListModule { }
