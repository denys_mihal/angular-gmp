import { ActionReducerMap } from '@ngrx/store';
import { coursesListReducer } from '../home-page/+state/courses-list.reducer';
import { Course } from '../core/common-classes/course';
import { userInformationReducer } from '../home-page/+state/user-information.reducer';

export interface State {
    coursesList: Course[],
    userInformation: {}
}

export const reducers: ActionReducerMap<State> = {
    coursesList: coursesListReducer,
    userInformation: userInformationReducer
};
